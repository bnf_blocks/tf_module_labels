# tf_module_labels

A module to keep tagging consistency across projects.  

## Example Use

```
module "labels" {
  source          = "git::https://gitlab.com/bnf_blocks/tf_module_labels.git?ref=v0.1.0"

  project_name    = "foo"
  environment     = "foo"
  owner           = "foo"
  project_url     = "foo"
  tf_stack_name   = "foo"
  tf_state_bucket = "foo"
  tf_state_key    = "foo"
  additional_tags = {}
}

resource "foo" "bar" {
	[...]

  tags = module.labels.tags
}
```
