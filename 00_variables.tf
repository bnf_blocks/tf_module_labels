variable "project_name" {
  type        = string
  description = "The project name"
}
variable "environment" {
  type        = string
  description = "Environment, e.g. 'prod', 'staging', 'dev'"
}
variable "owner" {
  type        = string
  description = "Owner of the resource"
}
variable "project_url" {
  type        = string
  description = "The url where to find the project (and the associated terraform manifest)"
}
variable "tf_stack_name" {
  type        = string
  description = "The name of the terraform stack"
}
variable "tf_state_bucket" {
  type        = string
  description = "The s3 bucket where statefile is stored"
}
variable "tf_state_key" {
  type        = string
  description = "The s3 key where statefile is stored"
}
variable "additional_tags" {
  type        = map(string)
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit','XYZ')`"
}
