locals {
  common_tags = merge(
    map(
      "project_name", "${var.project_name}",
      "environment", "${var.environment}",
      "owner", "${var.owner}",
      "project_url", "${var.project_url}",
      "tf_stack_name", "${var.tf_stack_name}",
      "tf_state_bucket", "${var.tf_state_bucket}",
      "tf_state_key", "${var.tf_state_key}"
    ),
    var.additional_tags
  )
}
