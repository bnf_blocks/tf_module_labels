output "tags" {
  value       = local.common_tags
  description = "Normalized Tag map"
}
